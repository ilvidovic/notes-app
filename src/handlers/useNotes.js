import React, { useMemo } from "react";

import * as actions from "../store/actions/noteActions";

import { NoteContext } from "../context/NoteContext";

export const useNotes = () => {
  const { state, dispatch } = React.useContext(NoteContext);

  return useMemo(
    () => ({
      get: (id) => state.notes[id],
      getAllIds: () => Object.keys(state.notes),
      getNoteCount: () => state.noteCount,
      save: (id, source) => {
        dispatch({ type: actions.NOTE_SAVE, payload: { id, source } });
      },
      add: (source) => {
        dispatch({ type: actions.NOTE_ADD, payload: { source } });
      },
      remove: (id) => {
        dispatch({ type: actions.NOTE_REMOVE, payload: { id } });
      },
      /**
       * details dialog related methods
       */
      openDialog: (id) => {
        dispatch({
          type: actions.NOTE_OPEN_DIALOG,
          payload: { id },
        });
      },
      closeDialog: () => {
        dispatch({
          type: actions.NOTE_CLOSE_DIALOG,
        });
      },
      isDialogOpen: () => state.dialogOpen,
      getSelectedNoteId: () => state.selectedNoteId,
    }),
    [state, dispatch]
  );
};
