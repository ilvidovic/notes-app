import { useEffect, useReducer } from "react";

import { NoteContext } from "../context/NoteContext";
import * as actions from "../store/actions/noteActions";

import {
  noteReducer,
  noteReducerDefaultValue,
} from "../store/reducers/noteReducer";

export const NoteProvider = (props) => {
  const [state, dispatch] = useReducer(noteReducer, noteReducerDefaultValue);

  useEffect(() => {
    // load notes from local storage on mount
    dispatch({ type: actions.NOTE_LOAD });
  }, []);

  const providerValue = { state, dispatch };

  return (
    <NoteContext.Provider value={providerValue}>
      {props.children}
    </NoteContext.Provider>
  );
};
