export const NOTE_CARD_HEIGHT_PX = "250px";
export const NOTE_CARD_WIDTH_PX = "250px";

export const NOTES_LOCALSTORAGE_KEY = "notesApp-notes";

export const DEFAULT_NOTE_TEXT =
  "This is a note\n==============\n\nSubtitle\n--------\n\n\n\nShopping list:\n* apples\n* orange\n* toilet paper\n";

export const NOTE_DIALOG_TEXTAREA_MIN_ROWS = 15;
