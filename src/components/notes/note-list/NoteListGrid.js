import { ButtonBase, Grid } from "@material-ui/core";

import { useNotes } from "../../../handlers/useNotes";

import NewNoteCard from "./NewNoteCard";
import NoteCard from "./NoteCard";

function NoteListGrid() {
  const notes = useNotes();

  return (
    <div className="main-grid-padding">
      <Grid container spacing={10} className="note-list-grid-padding">
        <Grid item>
          <ButtonBase
            onClick={() => {
              notes.openDialog();
            }}
          >
            <NewNoteCard />
          </ButtonBase>
        </Grid>
        {notes.getAllIds().map((noteId) => (
          <Grid item key={noteId}>
            <ButtonBase
              onClick={() => {
                notes.openDialog(noteId);
              }}
            >
              <NoteCard noteId={noteId} />
            </ButtonBase>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

export default NoteListGrid;
