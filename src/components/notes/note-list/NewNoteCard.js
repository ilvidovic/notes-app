import React from "react";
import { Card, CardContent, Grid, makeStyles } from "@material-ui/core";

import AddIcon from "@material-ui/icons/AddOutlined";

import {
  NOTE_CARD_HEIGHT_PX,
  NOTE_CARD_WIDTH_PX,
} from "../../../util/constants";

const useStyles = makeStyles({
  root: {
    height: NOTE_CARD_HEIGHT_PX,
    width: NOTE_CARD_WIDTH_PX,
    borderRadius: "15px",
    // backgroundColor: "#b90445",
    backgroundColor: "rgba(185, 4, 69, 1)",
    "&:hover": {
      backgroundColor: "rgba(185, 4, 69, 0.85)",
    },
  },
  cardContent: {
    height: "100%",
  },
  cardContentGrid: {
    height: "100%",
  },
  icon: {
    fontSize: "70px",
    color: "white",
  },
});

const NewNoteCard = React.memo(function NewNoteCard() {
  const classes = useStyles();

  return (
    <Card variant="outlined" className={classes.root}>
      <CardContent className={classes.cardContent}>
        <Grid
          container
          justify="center"
          alignItems="center"
          className={classes.cardContentGrid}
        >
          <Grid item>
            <AddIcon className={classes.icon} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
});

export default NewNoteCard;
