import React, { useMemo } from "react";
import { Card, CardContent, Grid, makeStyles } from "@material-ui/core";

import {
  NOTE_CARD_HEIGHT_PX,
  NOTE_CARD_WIDTH_PX,
} from "../../../util/constants";

import { useNotes } from "../../../handlers/useNotes";
import NoteTextMarkdown from "../note-details/NoteTextMarkdown";

const useStyles = makeStyles({
  root: {
    height: NOTE_CARD_HEIGHT_PX,
    width: NOTE_CARD_WIDTH_PX,
    borderRadius: "15px",
    boxShadow: "0px 0px 10px #e9e9e9",
    border: "3px solid transparent",
    "&:hover": {
      border: "3px solid #b90445",
    },
    textAlign: "initial",
    whiteSpace: "nowrap",
  },
  cardContent: {
    height: "100%",
  },
  cardContentGrid: {
    height: "100%",
    overflow: "clip",
  },
});

function NoteCard({ noteId }) {
  const classes = useStyles();
  const notes = useNotes();

  const note = notes.get(noteId);

  return useMemo(
    () => (
      <Card variant="outlined" className={classes.root}>
        <CardContent className={classes.cardContent}>
          <Grid container className={classes.cardContentGrid}>
            <Grid item xs={12}>
              {note && (
                <NoteTextMarkdown scale={0.25}>
                  {note.noteText}
                </NoteTextMarkdown>
              )}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    ),
    [note, classes]
  );
}

export default NoteCard;
