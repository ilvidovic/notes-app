import React from "react";
import { makeStyles } from "@material-ui/core";
import ReactMarkdown from "react-markdown";

const useStyles = makeStyles({
  root: {
    transform: (scale) => `scale(${scale || 1})`,
    transformOrigin: "top left",
    // fontSize: "0.25rem",
  },
});

const NoteTextMarkdown = React.memo(function NoteTextMarkdown({
  scale,
  ...props
}) {
  const classes = useStyles(scale);

  return (
    <ReactMarkdown className={classes.root}>{props.children}</ReactMarkdown>
  );
});

export default NoteTextMarkdown;
