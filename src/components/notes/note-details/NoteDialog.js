import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  makeStyles,
  TextareaAutosize,
} from "@material-ui/core";

import { ArrowBack, Delete, Edit, Save } from "@material-ui/icons";
import { useEffect, useState } from "react";

import { useNotes } from "../../../handlers/useNotes";
import {
  DEFAULT_NOTE_TEXT,
  NOTE_DIALOG_TEXTAREA_MIN_ROWS,
} from "../../../util/constants";

import NoteTextMarkdown from "./NoteTextMarkdown";

const useStyles = makeStyles({
  paper: {
    height: "100%",
  },
  icon: {
    "&:hover": {
      color: "#b90445",
      cursor: "pointer",
    },
  },
  textarea: {
    width: "100%",
    marginRight: "24px",
    marginBottom: "8px",
  },
});

export default function NoteDialog() {
  const classes = useStyles();
  const notes = useNotes();

  const isDialogOpen = notes.isDialogOpen();
  const selectedNoteId = notes.getSelectedNoteId();

  const [noteText, setNoteText] = useState(DEFAULT_NOTE_TEXT);
  const [isEditMode, setIsEditMode] = useState(false);
  const [lastSelectedNoteId, setLastSelectedNoteId] = useState(null);

  // change default note content based on selectedNoteId
  useEffect(() => {
    if (lastSelectedNoteId !== selectedNoteId) {
      if (selectedNoteId) {
        setNoteText(notes.get(selectedNoteId)?.noteText);
      } else {
        setNoteText(DEFAULT_NOTE_TEXT);
      }
      setLastSelectedNoteId(selectedNoteId);
    }
  }, [notes, selectedNoteId, lastSelectedNoteId]);

  // change edit mode based on selectedNoteId
  useEffect(() => {
    setIsEditMode(selectedNoteId ? false : true);
  }, [selectedNoteId]);

  const saveNote = () => {
    if (selectedNoteId) {
      // existing note
      notes.save(selectedNoteId, noteText);
    } else {
      // new note
      notes.add(noteText);
    }

    setIsEditMode(false);
  };

  const enableEditMode = () => {
    setIsEditMode(true);
  };

  const deleteNote = () => {
    /**
     * remove stored note if it's an existing one,
     * otherwise just close the dialog
     */
    if (selectedNoteId) {
      notes.remove(selectedNoteId);
    }

    notes.closeDialog();
  };

  return (
    <Dialog
      fullWidth
      maxWidth="md"
      classes={{ paper: classes.paper }}
      open={isDialogOpen}
      onClose={() => notes.closeDialog()}
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle>
        <Grid container justify="space-between" direction="row">
          <Grid item>
            <ArrowBack
              className={classes.icon}
              onClick={() => notes.closeDialog()}
            />
          </Grid>
          <Grid item>
            <Grid container spacing={2}>
              {isEditMode ? (
                <Grid item>
                  <Save className={classes.icon} onClick={() => saveNote()} />
                </Grid>
              ) : (
                <Grid item>
                  <Edit
                    className={classes.icon}
                    onClick={() => enableEditMode()}
                  />
                </Grid>
              )}
              <Grid item>
                <Delete className={classes.icon} onClick={() => deleteNote()} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <Grid container direction="column">
          <Grid item xs={12}>
            {isEditMode ? (
              <TextareaAutosize
                defaultValue={noteText}
                onChange={(e) => setNoteText(e.target.value)}
                rowsMin={NOTE_DIALOG_TEXTAREA_MIN_ROWS}
                className={classes.textarea}
              />
            ) : (
              <NoteTextMarkdown>{noteText}</NoteTextMarkdown>
            )}
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
}
