import NoteDialog from "./notes/note-details/NoteDialog";
import NoteListGrid from "./notes/note-list/NoteListGrid";

export default function Notes() {
  return (
    <>
      <NoteDialog />
      <NoteListGrid />
    </>
  );
}
