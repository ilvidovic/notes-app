import "./css/App.css";

import { NoteProvider } from "./handlers/NoteProvider";

import Notes from "./components/Notes";

function App() {
  return (
    <NoteProvider>
      <Notes />
    </NoteProvider>
  );
}

export default App;
