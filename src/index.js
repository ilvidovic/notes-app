import React from "react";
import ReactDOM from "react-dom";

import "./css/index.css";

import { CssBaseline } from "@material-ui/core";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  /**
   * Note:
   * "Warning: findDOMNode is deprecated in StrictMode."
   *  Strict mode console warning is due to Material-UI issue:
   *  https://github.com/mui-org/material-ui/issues/13394
   *  that is due to be resolved in v5
   */
  <React.StrictMode>
    <CssBaseline />
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
