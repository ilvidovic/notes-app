export const NOTE_LOAD = "NOTE_LOAD";
export const NOTE_SAVE = "NOTE_SAVE";
export const NOTE_ADD = "NOTE_ADD";
export const NOTE_REMOVE = "NOTE_REMOVE";

export const NOTE_OPEN_DIALOG = "NOTE_OPEN_DIALOG";
export const NOTE_CLOSE_DIALOG = "NOTE_CLOSE_DIALOG";
