import * as actions from "../actions/noteActions";

import { NOTES_LOCALSTORAGE_KEY } from "../../util/constants";

const noteDialogDefaultValue = {
  dialogOpen: false,
  selectedNoteId: null,
};

export const noteReducerDefaultValue = {
  // dialog/modal related data
  ...noteDialogDefaultValue,
  // note data
  noteCount: 0,
  noteIdCounter: 0,
  notes: {},
};

export const noteReducer = (state, action) => {
  let newState = null;

  switch (action.type) {
    /**
     * Note load
     */
    case actions.NOTE_LOAD:
      const storedNotes = localStorage.getItem(NOTES_LOCALSTORAGE_KEY);

      if (storedNotes) {
        return {
          ...JSON.parse(storedNotes),
          // overwrite stored dialog related data
          ...noteDialogDefaultValue,
        };
      } else {
        return state;
      }
    /**
     * Note save
     */
    case actions.NOTE_SAVE:
      newState = {
        ...state,
        notes: {
          ...state.notes,
          [action.payload.id]: {
            ...state.notes[action.payload.id],
            noteText: action.payload.source,
          },
        },
      };

      localStorage.setItem(NOTES_LOCALSTORAGE_KEY, JSON.stringify(newState));

      return newState;
    /**
     * Note add
     */
    case actions.NOTE_ADD:
      const newNoteId = state.noteIdCounter + 1;

      newState = {
        ...state,
        noteCount: state.noteCount + 1,
        noteIdCounter: newNoteId,
        notes: {
          ...state.notes,
          [newNoteId]: {
            noteText: action.payload.source,
          },
        },
        // change id of selected note in dialog
        selectedNoteId: newNoteId,
      };

      localStorage.setItem(NOTES_LOCALSTORAGE_KEY, JSON.stringify(newState));

      return newState;
    /**
     * Note remove
     */
    case actions.NOTE_REMOVE:
      const { [action.payload.id]: deletedNote, ...otherNotes } = state.notes;

      newState = {
        ...state,
        noteCount: state.noteCount - 1,
        notes: {
          ...otherNotes,
        },
      };

      localStorage.setItem(NOTES_LOCALSTORAGE_KEY, JSON.stringify(newState));

      return newState;
    /**
     * Note open dialog
     */
    case actions.NOTE_OPEN_DIALOG:
      return {
        ...state,
        dialogOpen: true,
        selectedNoteId: action.payload.id || null,
      };
    /**
     * Note close dialog
     */
    case actions.NOTE_CLOSE_DIALOG:
      return {
        ...state,
        dialogOpen: false,
      };
    /**
     * default
     */
    default:
      return state;
  }
};
